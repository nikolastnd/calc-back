# -*- coding: utf-8 -*-

import marshmallow as ma
from math_operations import math_operations


class Calculation:
    """Execute a math function.

    Args:
      operation: operation to be executed
                can be any of:
                - add
                - subtract
                - divide
                - multiply
      first_value: 1st value to be used in the math operation
      second_value: 2nd value to be used in the math operation


    Raises:
      ZeroDivisionError: if the second value of a division is zero

    """

    def __init__(self, operation: str, first: float, second: float) -> float:
        self._operation = operation
        self._first = float(first)
        self._second = float(second)
        self._decimal_fields = 4

    @property
    def result(self):
        return self._calc()

    def _calc(self):
        return round(math_operations[self._operation](
            self._first, self._second), self._decimal_fields)


class CalculationSchema(ma.Schema):
    operation = ma.fields.Str(required=True)
    first_value = ma.fields.Number(required=True)
    second_value = ma.fields.Number(required=True)
    result = ma.fields.Number()
