# -*- coding: utf-8 -*-


def addition(v1, v2):
    """ Sum of two numbers

    Args:
        v1 (float): first value for the operation
        v2 (float): second value for the operation

    Returns:
        float: sum of v1 and v2
    """
    return v1 + v2


def subtraction(v1, v2):
    """ Subtraction of two numbers

    Args:
        v1 (float): first value for the operation
        v2 (float): second value for the operation

    Returns:
        float: difference of v1 and v2
    """
    return v1 - v2


def division(v1, v2):
    """ Summ of two numbers

    Args:
        v1 (float): first value for the operation
        v2 (float): second value for the operation

    Returns:
        float: quotiont of v1 by v2
    """
    return v1 / v2


def multiplication(v1, v2):
    """ Summ of two numbers

    Args:
        v1 (float): first value for the operation
        v2 (float): second value for the operation

    Returns:
        float: product of v1 and v2
    """
    return v1 * v2


math_operations = {
    'add': addition,
    'subtract': subtraction,
    'divide': division,
    'multiply': multiplication
}
