
from base import BaseTest
from models import Calculation


class TestCalc(BaseTest):

    def test_sum(self):
        # Given

        first, second = [10, 4.5]
        operation = 'add'

        # When
        result = Calculation(operation, first, second).result

        # Then
        self.assertEqual(result, 14.5)

    def test_subtract(self):
        # Given

        first, second = [10, 4.5]
        operation = 'subtract'

        # When
        result = Calculation(operation, first, second).result

        # Then
        self.assertEqual(result, 5.5)

    def test_divide(self):
        # Given

        first, second = [10, 4.5]
        operation = 'divide'

        # When
        result = Calculation(operation, first, second).result

        # Then
        self.assertEqual(result, 2.2222)

    def test_multiply(self):
        # Given

        first, second = [10, 4.5]
        operation = 'multiply'

        # When
        result = Calculation(operation, first, second).result

        # Then
        self.assertEqual(result, 45)
