# -*- coding: utf-8 -*-

from http import HTTPStatus


def test_health(client):
    response = client.get('/health/')
    assert response.status_code == HTTPStatus.OK, 'Health check failed'
    assert response.json == {'message': 'Servicea is healthy'}, 'Improper response'
