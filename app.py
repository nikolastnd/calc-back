# -*- coding: utf-8 -*-

import os
import sentry_sdk
from flask import Flask
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from sentry_sdk.integrations.flask import FlaskIntegration
from resources import CalculationResource
from flask_cors import CORS

# APP CONFIG
sentry_sdk.init(
    dsn=
    "https://edc3140b1ca4466bbb9b079d062b60bc@o379938.ingest.sentry.io/5287408",
    integrations=[FlaskIntegration()])
app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
cors = CORS(app, resources={r"/calc/*": {"origins": "*"}})
docs = FlaskApiSpec(app)

app.config.update({
    'APISPEC_SPEC':
    APISpec(
        title='calculation',
        version='v1',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0.2',
    ),
    'APISPEC_SWAGGER_URL':
    '/swagger/',
})

# APP ROUTES
app.add_url_rule('/calc/<operation>', 'calc',
                 CalculationResource.as_view('CalculationResource'))


@app.route('/health', methods=['GET'])
def health():
    try:
        return {'message': 'Service is healthy'}, 200
    except ConnectionError:
        return {'message': 'Health check failed'}, 500


@app.route('/debug-sentry')
def trigger_error():
    division_by_zero = 1 / 0


# APP DOCS
# /swagger
docs.register(CalculationResource, 'calc')
docs.register(health, 'health')

if __name__ == '__main__':
    print(os.environ['APP_SETTINGS'])
    app.run()
