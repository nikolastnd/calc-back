define USAGE
Calculator service

Commands:
	init      Install Python dependencies with pipenv
	test      Run linters, test db migrations and tests.
	serve     Run app in dev environment.
endef

export USAGE
help:
	@echo "$$USAGE"

init:
	pip3 install pipenv
	pipenv install --dev --skip-lock

test:
	pipenv run python -m unittest discover tests

serve:
	pipenv run python app.py