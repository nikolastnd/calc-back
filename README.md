# Setup application
Install dependencies
>$ make init

Run tests
>$ make test

Start application
>$ make serve