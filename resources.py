#!/usr/bin/python
# -*- coding: utf-8 -*-

import six
from flask import views, request, abort
from flask_apispec import ResourceMeta, doc, marshal_with
from models import Calculation, CalculationSchema
from marshmallow import ValidationError


class MethodResourceMeta(ResourceMeta, views.MethodViewType):

    pass


class MethodResource(six.with_metaclass(MethodResourceMeta, views.MethodView)):

    methods = None


class CalculationResource(MethodResource):
    @marshal_with(CalculationSchema)
    def post(self, operation, **kwargs):
        first = request.json.get('first_value')
        second = request.json.get('second_value')

        try:
            CalculationSchema().load({
                'operation': operation,
                'first_value': first,
                'second_value': second
            })
            calc = Calculation(operation, first, second)
            return calc
        except (ValueError, NotImplementedError, ZeroDivisionError, ValidationError) as error:
            abort(400, error)
        except Exception as error:
            abort(500, error)
